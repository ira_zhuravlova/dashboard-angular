import ng from 'angular';

import './components/module.scss';
import 'bootstrap/dist/css/bootstrap.css';
import 'angular-ui-bootstrap';

import ngRoute from 'angular-route';
import Components from './components';

const app = ng.module('app', [
  Components,
  'ui.bootstrap',
  ngRoute
]);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
      .when('/dashboard', {
        name: 'dashboard',
        template: '<app-dashboard users-list="root.usersList"></app-dashboard>'
      })
      .when('/profile', {
        name: 'profile',
        template: '<app-profile></app-profile>'
      })
      .when('/profile/edit', {
        name: 'profile-edit',
        template: '<app-profile-edit users-list="root.usersList"></app-profile-edit>'
      })
      .when('/logout', {
        name: 'logout',
        template: '<h2>Logout</h2>'
      })
      .otherwise({
        redirectTo: '/dashboard'
      });
  }
]);


class AppRootController {
  constructor($scope, $http) {
    'ngInject';
    this.$scope = $scope;
    this.$http = $http;
  }

  $onInit() {
    this.setLocalStorageInfo();
    this.setLocationState();
  }

  setLocalStorageInfo() {
    let name = localStorage.getItem('login');
    let birthday = localStorage.getItem('birthday');
    let city = localStorage.getItem('city');
    this.usersList = localStorage.getItem('users');
    this.usersList = JSON.parse(this.usersList);

    if (!this.usersList) {
      this.getUsers();
    }

    if (!name || !birthday || !city) {
      localStorage.setItem('login', 'John');
      localStorage.setItem('birthday', '2008-07-06');
      localStorage.setItem('city', 'Budapest');
    }
  }

  setLocationState() {

    this.$scope.$on("$routeChangeSuccess", (event, current, previous) => {
      this.routeName = current.$$route.name;

      if (this.routeName === 'dashboard') {
        this.locationState = 'dashboard';

      } else if (this.routeName === 'profile' || this.routeName === 'profile-edit') {
        this.locationState = 'profile';

      } else if (this.routeName === 'logout') {
        this.locationState = 'logout';
      }
    });

  }

  getUsers() {

    this.$http({
      type: 'GET',
      url: '/json/users.json'
    }).then((res) => {
        let resJSON = JSON.stringify(res.data);
        localStorage.setItem('users', resJSON);
        this.usersList = res.data;
      },
      (err) => {
        console.log(err);
      });
  };
}

app.controller('RootController', ['$scope', '$http', AppRootController]);