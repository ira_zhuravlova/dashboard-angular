import template from './app-dashboard-template.html';
import controller from './controllers';

export default {
  template,
  controller,
  restrict: 'E',
  controllerAs: 'appDashboardCtrl',
  bindings: {
    usersList: '='
  }

};
