import ng from 'angular';
import AppDashboardComponent from './components';

export default ng.module('app.components.appDashboard', [])
    .component('appDashboard', AppDashboardComponent)
    .name;
