export default class AppDashboardController {
  constructor() {
    'ngInject';

  }

  saveUserState(event) {
    let checkboxState = event.target.checked,
      currentUserCell = angular.element(event.target).parent().parent().children()[1],
      currentUserName = angular.element(currentUserCell).html();

    let oldUserInfo = this.usersList.find((item) => item.name === currentUserName);
    oldUserInfo.activeStatus = checkboxState;
    this.setLocalStorageInfo();

  }

  saveUserRadioState(event) {
    let checkboxState = event.target.checked,
      currentUserCell = angular.element(event.target).parent().parent().children()[1],
      currentUserName = angular.element(currentUserCell).html();

    this.usersList.forEach((item, index, arr) => {
      if (item.name === currentUserName) {
        item.bossStatus = true;
      } else {
        item.bossStatus = false;
      }
    });

    this.setLocalStorageInfo();

  }

  setLocalStorageInfo() {
    let userJSON = JSON.stringify(this.usersList);
    localStorage.setItem('users', userJSON);
  }


}