import ng from 'angular';

import AppMenu from './app-menu';
import AppDashboard from './app-dashboard';
import AppProfile from './app-profile';
import AppProfileEdit from './app-profile-edit';


export default ng.module('app.components', [AppMenu, AppDashboard, AppProfile, AppProfileEdit]).name;
