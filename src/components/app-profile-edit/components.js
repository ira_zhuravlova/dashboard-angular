import template from './app-profile-edit-template.html';
import controller from './controllers';

export default {
  template,
  controller,
  restrict: 'E',
  controllerAs: 'appProfileEditCtrl',
   bindings: {
     usersList: '='
  }
};
