import ng from 'angular';

import AppProfileEditComponent from './components';

export default ng.module('app.components.appProfileEdit', ['ui.bootstrap'])
    .component('appProfileEdit', AppProfileEditComponent)
    .name;
