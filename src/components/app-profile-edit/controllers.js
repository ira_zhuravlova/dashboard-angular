export default class AppProfileEditController {
  constructor($scope) {
    'ngInject';
    this.$scope = $scope;
  }

  $onInit() {
    this.getLocalStorageInfo();
    this.successMessage = false;
    this.errorMessage = false;
  }

  getLocalStorageInfo(){
    this.$scope.name = localStorage.getItem('login');
    this.$scope.birthday = new Date(localStorage.getItem('birthday'));
    this.$scope.city = localStorage.getItem('city');
  }

  saveUserInfo() {
    let userInfoArr = [this.$scope.name, this.$scope.birthday, this.$scope.city];

    if (userInfoArr.some(val => val === '')) {
      this.successMessage = false;
      this.errorMessage = true;
    } else {
      this.successMessage = true;
      this.errorMessage = false;

      let userName = localStorage.getItem('login');
      let currentUserInfo = this.usersList.find((item) => item.name === userName);

      currentUserInfo.name = this.$scope.name;
      currentUserInfo.birthday = this.$scope.birthday;
      currentUserInfo.city = this.$scope.city;

      this.setLocalStorageInfo();
      
    }

  }

  setLocalStorageInfo(){
    let userJSON = JSON.stringify(this.usersList);
      localStorage.setItem('users', userJSON);
      localStorage.setItem('login', this.$scope.name);
      localStorage.setItem('birthday', this.$scope.birthday);
      localStorage.setItem('city', this.$scope.city);
  }



}