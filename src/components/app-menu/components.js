import template from './app-menu-template.html';
import controller from './controllers';

export default {
  template,
  controller,
  restrict: 'E',
  controllerAs: 'appMenuCtrl',
   bindings: {
     locationState: '='
  }
};
