import ng from 'angular';

import AppProfileComponent from './components';

export default ng.module('app.components.appProfile', ['ui.bootstrap'])
    .component('appProfile', AppProfileComponent)
    .name;
