import template from './app-profile-template.html';
import controller from './controllers';

export default {
  template,
  controller,
  restrict: 'E',
  controllerAs: 'appProfileCtrl',
   bindings: {
    
  }
};
