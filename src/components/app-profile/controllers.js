export default class AppProfileController {
  constructor() {
    'ngInject';

  }

  $onInit() {
      this.getLocalStorageInfo();
  }

  getLocalStorageInfo(){
      this.userLogin = localStorage.getItem('login');
      this.userBirthday = new Date(localStorage.getItem('birthday'));
      this.userCity = localStorage.getItem('city');
  }

}